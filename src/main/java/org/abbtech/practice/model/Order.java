package org.abbtech.practice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity
@Table(name = "order", schema = "book_delivery")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Order {
    @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GeneratedValue(generator = "UUID")
    @Column(name = "id", nullable = false)
    private UUID id;
    @Column(name = "book_id")
    private UUID bookId;
    private int quantity;
    @Column(name = "user_id")
    private UUID userId;
}
