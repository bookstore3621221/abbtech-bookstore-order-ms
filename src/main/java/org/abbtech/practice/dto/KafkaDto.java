package org.abbtech.practice.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KafkaDto {
    private String orderId;
    private String userEmail;
}
