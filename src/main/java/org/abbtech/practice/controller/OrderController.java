package org.abbtech.practice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.OrderRequestDTO;
import org.abbtech.practice.model.Order;
import org.abbtech.practice.service.OrderService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
@Tag(name = "order for Only USER roles",description = " creating  confirming and getting order")
public class OrderController {
    private final OrderService orderService;

    @PostMapping
    @Operation(summary = "Create a new order", description = "Creates a new order based on the provided order details.")
    public Order createOrder(@RequestHeader(value = "X-USER-ID", required = false) String userId,
                             @RequestHeader(value = "X-USER-EMAIL", required = false) String userEmail,
                             @RequestBody OrderRequestDTO orderRequestDTO) {
        return orderService.createOrder(orderRequestDTO);
    }

    @PostMapping("/confirm")
    @Operation(summary = "Confirming order by its id", description = "Confirms an existing order based on the provided order details.")
    public void confirmOrder(@RequestHeader(value = "X-USER-ID", required = false) String userId,
                             @RequestHeader(value = "X-USER-EMAIL", required = false) String userEmail,
                             @RequestBody OrderRequestDTO order) {
        orderService.confirmOrder(order);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get order by ID", description = "Retrieves an order by its ID.")
    public Order getOrderbyId(@RequestHeader(value = "X-USER-ID", required = false) String userId,
                              @RequestHeader(value = "X-USER-EMAIL", required = false) String userEmail,
                              @PathVariable("id") String orderId) {
        return orderService.getOrderById(orderId);
    }
}
