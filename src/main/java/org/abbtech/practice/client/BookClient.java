package org.abbtech.practice.client;

import org.abbtech.practice.dto.BookResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;


@FeignClient(name = "book",
        url = "http://localhost:8082/api/abbtech-bookstore-api-gw/books"
)
public interface BookClient {
    @GetMapping("/{bookId}")
    BookResponseDTO getBookById(@PathVariable UUID bookId);
}
