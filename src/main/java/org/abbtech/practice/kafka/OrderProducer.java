package org.abbtech.practice.kafka;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.KafkaDto;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OrderProducer {

    private final KafkaTemplate<String, KafkaDto> kafkaTemplate;

    public void sendToKafka(KafkaDto kafkaDto){
        kafkaTemplate.send("orderConfirmed",kafkaDto);
    }
}
