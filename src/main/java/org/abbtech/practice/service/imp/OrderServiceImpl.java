package org.abbtech.practice.service.imp;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.client.BookClient;
import org.abbtech.practice.dto.BookResponseDTO;
import org.abbtech.practice.dto.KafkaDto;
import org.abbtech.practice.dto.OrderRequestDTO;
import org.abbtech.practice.model.Order;
import org.abbtech.practice.kafka.OrderProducer;
import org.abbtech.practice.repository.OrderRepository;
import org.abbtech.practice.service.OrderService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final BookClient bookClient;
    private final OrderProducer orderProducer;

    @Override
    public Order createOrder(OrderRequestDTO orderRequestDTO) {
        BookResponseDTO bookResponse = bookClient.getBookById(UUID.fromString(orderRequestDTO.getBookId()));
        if (bookResponse == null) {
            throw new RuntimeException("Book not found");
        }
        Order order = new Order();
        //order.setId(UUID.randomUUID());
        order.setQuantity(orderRequestDTO.getQuantity());
        order.setUserId(UUID.fromString(orderRequestDTO.getUserId()));
        order.setBookId(UUID.fromString(orderRequestDTO.getBookId()));
        return orderRepository.save(order);

    }

    @Override
    public void confirmOrder(OrderRequestDTO orderRequest) {
        //TODO  publish event over Apache KAFKA
        UUID id = UUID.fromString(orderRequest.getOrderId());
        Order order = orderRepository.findById(id).orElseThrow(() -> new RuntimeException("Order not found"));
        KafkaDto kafkaDto = KafkaDto.builder()
                .orderId(orderRequest.getOrderId())
                .userEmail("murad@gmail.com")
                .build();

        orderProducer.sendToKafka(kafkaDto);
        orderRepository.save(order);
    }

    @Override
    public Order getOrderById(String orderId) {
        UUID id = UUID.fromString(orderId);
        return orderRepository.findById(id).orElseThrow(() -> new RuntimeException("Order not found"));
    }
}
