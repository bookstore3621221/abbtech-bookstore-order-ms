package org.abbtech.practice.service;

import org.abbtech.practice.dto.OrderRequestDTO;
import org.abbtech.practice.model.Order;

import java.util.UUID;

public interface OrderService  {
    Order createOrder(OrderRequestDTO orderRequestDTO);
    void confirmOrder(OrderRequestDTO order);
    Order getOrderById(String id);
}
